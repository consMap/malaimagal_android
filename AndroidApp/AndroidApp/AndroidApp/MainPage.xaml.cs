﻿using System.Collections.ObjectModel;
using Xamarin.Forms;
using AndroidApp.Library;
using AndroidApp.ViewModel;
using System.Linq;

namespace AndroidApp
{
    public partial class MainPage : ContentPage
    {
        ObservableCollection<ListViewModel> ItemSource;
        DataMapper Source;
        public MainPage(DataMapper source)
        {
            InitializeComponent();
            Source = source;
            ItemSource = new ObservableCollection<ListViewModel>(Source.DataStore);
            ListDisplay.ItemsSource = ItemSource;
        }

        private void SearchBar_TextChanged(object sender, TextChangedEventArgs e)
        {
            if(string.IsNullOrEmpty(e.NewTextValue) || string.IsNullOrEmpty(e.OldTextValue))
            {
                ListDisplay.ItemsSource = ItemSource;
            }
            else
            {
                var newList = new ObservableCollection<ListViewModel>(Source.DataStore.Where(item => item.itemName.Contains(e.NewTextValue.ToUpper())));
                ListDisplay.ItemsSource = newList;
            }
        }
    }
}

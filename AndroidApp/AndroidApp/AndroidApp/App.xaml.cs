﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AndroidApp.Library;

using Xamarin.Forms;

namespace AndroidApp
{
    public partial class App : Application
    {
        public App(string json)
        {
            InitializeComponent();
            var input = new DataMapper(json);
            MainPage = new AndroidApp.MainPage(input);
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}

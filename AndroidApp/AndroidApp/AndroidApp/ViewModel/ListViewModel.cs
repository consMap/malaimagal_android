﻿namespace AndroidApp.ViewModel
{
    public class ListViewModel
    {
        public string itemName { get; set; }
        public string rack { get; set; }
        public string date { get; set; }
        public string sellingPrice { get; set; }
        public string code { get; set; }
    }
}

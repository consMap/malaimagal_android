﻿using System.Collections.Generic;
using System.Linq;
using AndroidApp.Model;
using AndroidApp.ViewModel;
using Newtonsoft.Json;

namespace AndroidApp.Library
{
    public class DataMapper
    {
        public List<ListViewModel> DataStore { get; private set; }

        public DataMapper(string json)
        {
            var collection = JsonConvert.DeserializeObject<List<DataStoreModel>>(json);
            DataStore = (from data in collection
                         select new ListViewModel
                         {
                             code = data.Code,
                             date = data.Date,
                             itemName = data.ItemName.ToUpper(),
                             rack = data.Rack,
                             sellingPrice = data.Price
                         }).ToList();
        }
    }
}

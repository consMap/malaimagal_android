﻿namespace AndroidApp.Model
{
    public class DataStoreModel
    {
        public string ItemName { get; set; }
        public string Rack { get; set; }
        public string Date { get; set; }
        public string Qty { get; set; }
        public string Price { get; set; }
        public string Code { get; set; }
    }
}

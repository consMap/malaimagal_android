﻿using Bytescout.Spreadsheet;
using System.Configuration;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.IO;

namespace Excel2Json
{
    static class Program
    {
        static readonly Worksheet _excelStore;
        static Program()
        {
            _excelStore = loadExcelFile();
        }
        static Worksheet loadExcelFile()
        {
            var excelPath = string.Format("{0}Malaimagal.xlsx", ConfigurationManager.AppSettings["excelPath"]);
            var excel = new Spreadsheet();
            excel.LoadFromFile(excelPath);
            return excel.Workbook.Worksheets.ByName("Sheet1");
        }
        static List<model> excelList()
        {
            var list = new List<model>();
            var maxRows = _excelStore.UsedRangeRowMax;
            for (int xIndex = 1; xIndex <= maxRows; xIndex++)
            {
                var row = new model
                {
                    ItemName = _excelStore.Cell(xIndex,0).ValueAsString,
                    Rack = _excelStore.Cell(xIndex, 1).ValueAsString,
                    Date = _excelStore.Cell(xIndex, 2).ValueAsString,
                    Qty = _excelStore.Cell(xIndex, 3).ValueAsString,
                    Price = _excelStore.Cell(xIndex, 4).ValueAsString,
                    Code = _excelStore.Cell(xIndex, 5).ValueAsString
                };
                if(!string.IsNullOrEmpty(row.ItemName))
                {
                    list.Add(row);
                }
            }
            return list;
        }
        static void generateJson()
        {
            var json = JsonConvert.SerializeObject(excelList());
            var jsonPath = string.Format("{0}AksJson.json", ConfigurationManager.AppSettings["jsonPath"]);
            using (var stream = new StreamWriter(jsonPath))
            {
                stream.Write(json);
            }
        }
        static void Main(string[] args)
        {
            Program.generateJson();
        }
    }
    class model
    {
        public string ItemName { get; set; }
        public string Rack { get; set; }
        public string Date { get; set; }
        public string Qty { get; set; }
        public string Price { get; set; }
        public string Code { get; set; }
    }
}
